const objectPath = require('object-path')

function getObject(obj, path, defaultValue) {
    if (objectPath.has(obj, path)) {
        return objectPath.get(obj, path);
    } else {
        return defaultValue;
    }
}

module.exports = {
    getObject
}